package mypack;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


import java.util.ArrayList;
import java.util.List;

@Controller
public class WebController {
    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/profile")
    public String profile(Model model) {
        List<Profile> list = new ArrayList<>();
        list.add(new Profile("fullname", "Nguyen Van Hung"));
        list.add(new Profile("username","hungnguyen"));
        model.addAttribute("list",list);
        return "profile";
    }
}
